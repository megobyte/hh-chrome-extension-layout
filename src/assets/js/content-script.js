async function init() {
  let response = await fetch(chrome.extension.getURL('/assets/js/injected.js'));
  if (response.ok) {
    let script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.innerHTML = await response.text();

    document.getElementsByTagName("head")[0].appendChild(script);
    document.getElementsByTagName("body")[0].setAttribute("onLoad", "huntica.init('"+chrome.extension.getURL('/')+"');");
  }
}

init();