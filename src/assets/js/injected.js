window.huntica = {
  async init(path) {
    let css = document.createElement("link");
    css.setAttribute("rel", "stylesheet");
    css.setAttribute("href", path + "assets/css/style.css");
    document.getElementsByTagName("head")[0].append(css);

    let hunticaDiv = document.createElement("div");
    hunticaDiv.setAttribute("id","hunticaChromeExtension");

    let div = document.createElement("div");
    let response = await fetch(path + 'inject.html');
    div.innerHTML = await response.text();

    console.log(div)

    hunticaDiv.append(div.querySelector("#hunticaBar"));
    hunticaDiv.append(div.querySelector("#hunticaContent"));

    document.getElementsByTagName("body")[0].prepend(hunticaDiv);

    document.querySelector(".resume-block .bloko-header-2").parentNode.prepend(div.querySelector("#hunticaBlackList"));
  }
}