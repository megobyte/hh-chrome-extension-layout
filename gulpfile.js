const gulp = require('gulp')

const serve = require('./gulp/tasks/serve')
const pug2html = require('./gulp/tasks/pug2html')
const styles = require('./gulp/tasks/styles')
const clean = require('./gulp/tasks/clean')
const copyManifest = require('./gulp/tasks/copyManifest')
const copyDependencies = require('./gulp/tasks/copyDependencies')

const dev = gulp.parallel(copyDependencies, copyManifest, pug2html, styles)

const build = gulp.series(clean, dev)

module.exports.start = gulp.series(build, serve)
module.exports.build = build
