const path = require("path");
const gulp = require("gulp");

module.exports = function copyDependencies(cb) {
  gulp
    .src('src/assets/js/**/*')
    .pipe(gulp.dest('build/assets/js'))
    .on("end", cb);
  gulp
    .src('src/assets/img/**/*')
    .pipe(gulp.dest('build/assets/img'))
    .on("end", cb);
  gulp
    .src('src/assets/fonts/**/*')
    .pipe(gulp.dest('build/assets/fonts'))
    .on("end", cb);
};
