const path = require("path");
const gulp = require("gulp");

module.exports = function copyManifest(cb) {
  gulp
    .src('src/manifest.json')
    .pipe(gulp.dest('build'))
    .on("end", cb);
};
