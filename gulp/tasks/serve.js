const gulp = require('gulp')

const styles = require('./styles')
const pug2html = require('./pug2html')
const copyManifest = require('./copyManifest')
const copyDependencies = require('./copyDependencies')

const server = require('browser-sync').create()

module.exports = function serve(cb) {
    server.init({
        server: 'build',
        notify: false,
        open: true,
        cors: true
    })

    gulp.watch('src/assets/scss/**/*.scss', gulp.series(styles, cb => gulp.src('build/assets/css').pipe(server.stream()).on('end', cb)))
    gulp.watch('src/assets/js/**/*', gulp.series(copyDependencies)).on('change', server.reload)
    gulp.watch('src/assets/img/**/*', gulp.series(copyDependencies)).on('change', server.reload)
    gulp.watch('src/pages/**/*.pug', gulp.series(pug2html))
    gulp.watch('src/manifest.json', gulp.series(copyManifest)).on('change', server.reload)
    gulp.watch('build/*.html').on('change', server.reload)

    // gulp.watch('package.json', gulp.series(copyDependencies)).on('change', server.reload)

    return cb()
}